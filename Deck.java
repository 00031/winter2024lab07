import java.util.Random;

public class Deck { 
private Card[] cards;
private int numberOfCards;
private Random rng;

//constructor 
public Deck () {
	this.rng = new Random(); 
	this.numberOfCards = 52;
	this.cards = new Card[52];
	String [] values = { "ACE", "TWO", "THREE", "FOUR", "FIVE","SIX", "SEVEN", "HEIGHT", "NINE" , "TEN", "JACK", 
	"QUEEN", "KING"};
	String [] suit = {"spades", "clubs", "hearts", "diamonds"};
	int counter = 0;
	for ( int i =0; i < suit.length; i++) {
		for (int y=0; y < values.length; y++){
			this.cards[counter] = new Card(values[y], suit[i]);
			counter ++;	
	}
}
}

	public int length() {
		return this.numberOfCards;
	}
	public Card drawTopCard () {
     this.numberOfCards --;
	 return cards[numberOfCards -1];
	} 
	public String toString () {
		String card ="";
		for( int i =0; i < this.numberOfCards; i++) {
			card += cards[i].toString() + "\n";
		}
		return card; 
	} 
	// shuffle method
	public void shuffle() { 
	 for (int i =0; i < cards.length; i++) {
		int position = this.rng.nextInt(52);
		Card placeholder = cards[position];
		cards[position] = cards[i];
		cards[i] = placeholder;
	 }
	} 
}
		 
		
		
