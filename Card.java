public class Card {
	private String suit;
	private String value;
	// get method	
	public String getSuit () {
		return this.suit;
	} 
	public String getValue (){
		return this.value;
	}

	// constructor
	public Card (String suit, String value) {
		this.suit = suit;
		this.value = value;
	  } 
	  // toString method
	public String toString () {
		return this.value + " of " + this.suit;
	}
	
	// method to calculate score
	public double calculateScore() {
    double score=0;
	double totalScore =0;
	if (this.suit.equals("hearts")) {
		score = 0.4;
	} else if (this.suit.equals("spades")) {
		score = 0.3;
	} else if ( this.suit.equals("diamonds")){
		score = 0.2;
	} else if ( this.suit.equals("clubs")){
		score = 0.1;
	} else {
	    score += 8;
	}
		
	if (this.value.equals("ACE")) {
		 totalScore = score + 1.0;
    } else if (this.value.equals("TW0")){
		 totalScore = score + 2.0;
	} else if (this.value.equals("THREE")){
		 totalScore = score + 3.0;
	} else if (this.value.equals("FOUR")) {
		 totalScore = score + 4.0;
	} else if (this.value.equals("FIVE")){
		 totalScore = score + 5.0;
	} else if (this.value.equals("SIX")){
		 totalScore = score + 6.0;
	} else if (this.value.equals("SEVEN")) {
		 totalScore = score + 7.0;
	} else if (this.value.equals("EIGHT")){
		 totalScore = score + 8.0;
	} else if (this.value.equals("NINE")){
		 totalScore = score + 9.0;
	} else if (this.value.equals("TEN")) {
		 totalScore = score + 10.0;
	} else if (this.value.equals("JACK")){
		 totalScore = score + 11.0;
	} else if (this.value.equals("QUEEN")){
		 totalScore = score + 12.0;
	} else if (this.value.equals("KING")) {
		 totalScore = score + 13.0;
	} else {
		totalScore = score + 0;
		
	}
	return totalScore;
	}
}
	
	